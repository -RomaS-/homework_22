package com.stolbunov.roman.homework_22.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.db.EmulatorDB;
import com.stolbunov.roman.homework_22.ui.adapters.UserMoviesAdapter;

public class TVShowsFragment extends AbstractTabFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv_shows, container, false);
        initRecyclerView(view,
                getAdapter(EmulatorDB.getInstance().getMovies()),
                getManager());
        return view;
    }

    private void initRecyclerView(View view, UserMoviesAdapter adapter, LinearLayoutManager manager) {
        RecyclerView recyclerView = view.findViewById(R.id.rv_fragment_tv_shows);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }

    public static TVShowsFragment newInstance(Context context) {
        Bundle args = new Bundle();
        TVShowsFragment fragment = new TVShowsFragment();
        fragment.setArguments(args);
        fragment.setTitle(context.getString(R.string.tv_shows));
        return fragment;
    }
}
