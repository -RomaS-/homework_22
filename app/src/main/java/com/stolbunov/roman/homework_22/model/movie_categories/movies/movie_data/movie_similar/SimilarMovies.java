package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_similar;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SimilarMovies implements Parcelable {
    private String movie1;
    private String movie2;
    private String movie3;
    private String movie4;

    public SimilarMovies(Collection<String> collectionUri) {
        List<String> list = new ArrayList<>(collectionUri);
        movie1 = list.get(0);
        movie2 = list.get(1);
        movie3 = list.get(2);
        movie4 = list.get(3);
    }

    public SimilarMovies(String movie1, String movie2, String movie3, String movie4) {
        this.movie1 = movie1;
        this.movie2 = movie2;
        this.movie3 = movie3;
        this.movie4 = movie4;
    }

    protected SimilarMovies(Parcel in) {
        movie1 = in.readString();
        movie2 = in.readString();
        movie3 = in.readString();
        movie4 = in.readString();
    }

    public static final Creator<SimilarMovies> CREATOR = new Creator<SimilarMovies>() {
        @Override
        public SimilarMovies createFromParcel(Parcel in) {
            return new SimilarMovies(in);
        }

        @Override
        public SimilarMovies[] newArray(int size) {
            return new SimilarMovies[size];
        }
    };

    public String getMovie1() {
        return movie1;
    }

    public String getMovie2() {
        return movie2;
    }

    public String getMovie3() {
        return movie3;
    }

    public String getMovie4() {
        return movie4;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(movie1);
        dest.writeString(movie2);
        dest.writeString(movie3);
        dest.writeString(movie4);
    }
}
