package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.image_types;

import java.util.LinkedList;
import java.util.List;

public class ImageTypeFactory {
    private static List<String> imagesURIVertical;
    private static List<String> imagesURIHorizontal;

    static {
        creatingImagesUriVertical();
        creatingImagesUriHorizontal();
    }

    public static ImageType createImageType(int indexMovie) {
        return new ImageType(imagesURIHorizontal.get(indexMovie), imagesURIVertical.get(indexMovie));
    }

    private static void creatingImagesUriVertical() {
        imagesURIVertical = new LinkedList<>();
        imagesURIVertical.add("https://www.kino-teatr.ru/movie/poster/127858/89076.jpg");
        imagesURIVertical.add("http://www.kinokadr.ru/filmzimg/b/blackpanther/blackpanther_poster3.jpg");
        imagesURIVertical.add("https://media.kg-portal.ru/movies/r/riseoftheguardians/posters/riseoftheguardians_8.jpg");
        imagesURIVertical.add("http://www.tuskenium.com/uploads/images/00/00/01/2018/05/21/123d131660.jpg");
        imagesURIVertical.add("https://bwahahacomics.ru/wp-content/uploads/2018/04/e0a3ac0b217e9c96bdf32d113bc1b651.jpg");
        imagesURIVertical.add("https://english-films.com/uploads/mini/250x325/ae/7bc9087d84ea325515d75312bcdc5f.jpg");
        imagesURIVertical.add("http://kinogo.cc/uploads/posts/2013-10/1381393952_teoriya7.jpg");
        imagesURIVertical.add("http://lostfilm.info/images/poster/548/5473029.jpg");
        imagesURIVertical.add("https://st.kp.yandex.net/im/poster/2/7/5/kinopoisk.ru-Warcraft-2752173.jpg");
        imagesURIVertical.add("https://pre00.deviantart.net/7fcf/th/pre/i/2017/208/2/3/transformers_universe__bumblebee_movie_poster_by_arkhamnatic-dbhwcng.png");
        imagesURIVertical.add("https://i.pinimg.com/originals/c4/14/57/c41457716992c96f459d454541c79144.jpg");
        imagesURIVertical.add("https://upload.wikimedia.org/wikipedia/en/thumb/b/bf/Transformers_dark_of_the_moon_ver5.jpg/220px-Transformers_dark_of_the_moon_ver5.jpg");
        imagesURIVertical.add("https://m.media-amazon.com/images/M/MV5BMTcyOTc2OTA1Ml5BMl5BanBnXkFtZTcwOTI1MjkzOQ@@._V1_.jpg");
        imagesURIVertical.add("http://www.kinokadr.ru/filmzimg/j/justgowithit/justgowithit_poster.jpg");
        imagesURIVertical.add("https://images-na.ssl-images-amazon.com/images/I/518jo3Xlf8L.jpg");
        imagesURIVertical.add("http://www.kinokadr.ru/filmzimg/b/blended/blended_poster2.jpg");
        imagesURIVertical.add("https://media.kg-portal.ru/movies/a/avatar/posters/avatar_10.jpg");
    }

    private static void creatingImagesUriHorizontal() {
        imagesURIHorizontal = new LinkedList<>();
        imagesURIHorizontal.add("https://i.ytimg.com/vi/LZKNAMHOu5I/maxresdefault.jpg");
        imagesURIHorizontal.add("https://media.comicbook.com/2017/11/black-panther-movie-poster-1059050-1280x0.jpg");
        imagesURIHorizontal.add("https://www.filmofilia.com/wp-content/uploads/2012/02/Rise-of-the-Guardians.jpg");
        imagesURIHorizontal.add("https://2uafx22j66o113y5v13l5cduqte-wpengine.netdna-ssl.com/wp-content/uploads/2018/05/1-Signed-Solo-A-Star-Wars-Story-Official-Movie-Poster-Sweepstakes.jpg");
        imagesURIHorizontal.add("https://dz7u9q3vpd4eo.cloudfront.net/wp-content/legacy/posts/8bfd2e65-73c9-4c25-b35f-cd067a587fbc.jpg");
        imagesURIHorizontal.add("https://www.sunhome.ru/i/wallpapers/66/film-doktor-haus-oboi.960x540.jpg");
        imagesURIHorizontal.add("https://www.allplayer.org/images/series/big-bang-theory.jpg");
        imagesURIHorizontal.add("https://pbs.twimg.com/media/Dn4WcUfVYAIVnOM.jpg");
        imagesURIHorizontal.add("http://darksite.az/wp-content/uploads/2016/04/worldofwarcraft_21.jpg");
        imagesURIHorizontal.add("https://www.prime1studio.com/media/catalog/product/cache/1/image/1400x1400/17f82f742ffe127f42dca9de82fb58b1/m/m/mmtfm-20_tomy_a10_tomy.jpg");
        imagesURIHorizontal.add("https://www.movies4kids.co.uk/wp-content/uploads/2016/04/Doctor-Strange-Movie-Mordo-and-Strange-Banner.jpg");
        imagesURIHorizontal.add("https://ae01.alicdn.com/kf/HTB12EOVKVXXXXciXVXXq6xXFXXXx/Free-shipping-Transformers-MOVIE-Poster-HD-HOME-WALL-Decor-Custom-ART-PRINT-Silk-Wallpaper-unframed-1478.jpg_640x640.jpg");
        imagesURIHorizontal.add("https://ae01.alicdn.com/kf/HTB1ecaYOFXXXXcRaXXXq6xXFXXXt/The-Croods-2-Movie-Poster-7-Home-Wall-Decor-Art-Silk-Canvas-Poster-Print-Painting-24x36.jpg_640x640.jpg");
        imagesURIHorizontal.add("http://chalemadame.com/wp-content/uploads/2015/06/just-go-with-it-503bf652a2cef.jpg");
        imagesURIHorizontal.add("http://myredcarpet.eu/wp-content/uploads/2018/10/Venom-Movie-Poster.jpg");
        imagesURIHorizontal.add("https://www.ultimateafrica.com/wp-content/uploads/2014/05/Blended-Movie-e1399323777588.jpg");
        imagesURIHorizontal.add("http://jonvilma.com/images/official-avatar-movie-poster-3.jpg");
    }

    public static List<String> getImagesUriVertical() {
        return imagesURIVertical;
    }
}
