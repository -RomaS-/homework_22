package com.stolbunov.roman.homework_22.model.movie_categories;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.MoviesFactory;

import java.util.LinkedList;
import java.util.List;

public class MovieCategoryFactory {
    private static List<String> categoryName;

    static {
        categoryName = new LinkedList<>();
        categoryName.add("New Movies");
        categoryName.add("Fantasy");
        categoryName.add("Cartoons");
        categoryName.add("Comedy");
        categoryName.add("Serials");
        categoryName.add("Recommended for you");
    }

    public static List<MovieCategory> createMovieCategories() {
        List<MovieCategory> movieCategories = new LinkedList<>();
        for (int i = 0; i < categoryName.size(); i++) {
            movieCategories.add(new MovieCategory(categoryName.get(i), MoviesFactory.createMovies()));
        }
        return movieCategories;
    }

    public static MovieCategory createOneMovieCategoryByTitle(String title) {
        return new MovieCategory(title, MoviesFactory.createMovies());
    }
}
