package com.stolbunov.roman.homework_22.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.db.EmulatorDB;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.model.movie_categories.MovieCategory;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_description.MovieDescription;
import com.stolbunov.roman.homework_22.ui.adapters.MovieCategoriesAdapter;
import com.stolbunov.roman.homework_22.ui.adapters.OnCategoryItemClickListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnCategoryItemClickListener {
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        createDrawerLayout(toolbar);
        initRecyclerView(getLayoutManager(), getMovieCategoriesAdapter());
        initNavigationView();
    }

    private void initRecyclerView(RecyclerView.LayoutManager layoutManager, MovieCategoriesAdapter movieCategoriesAdapter) {
        RecyclerView categoryContainer = findViewById(R.id.rv_category_container);
        categoryContainer.setAdapter(movieCategoriesAdapter);
        categoryContainer.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tb_main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nv_menu_my_library:
                goToSelectedItem(UserLibraryActivity.class);
                break;
        }
        return true;
    }

    private void goToSelectedItem(Class goToClass) {
        Intent intent = new Intent(this, goToClass);
        startActivity(intent);
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START, true);
        } else {
            super.onBackPressed();
        }
    }

    @NonNull
    private MovieCategoriesAdapter getMovieCategoriesAdapter() {
        MovieCategoriesAdapter adapter  = new MovieCategoriesAdapter(
                EmulatorDB.getInstance().getMovitCategory());
        adapter.setCategoryItemClickListener(this);

        return adapter;
    }

    @NonNull
    private RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);
    }

    private void initNavigationView() {
        NavigationView navigationView = findViewById(R.id.main_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void createDrawerLayout(Toolbar toolbar) {
        drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public void categoryChildItemClick(Movie movie) {
        Intent intent = new Intent(this, MovieDescriptionActivity.class);
        intent.putExtra(MovieDescriptionActivity.KEY_MOVIE_INTENT, movie);
        startActivity(intent);
    }

    @Override
    public void categoryItemClick(MovieCategory category) {

    }
}
