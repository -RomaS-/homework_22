package com.stolbunov.roman.homework_22.model.db;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.model.movie_categories.MovieCategory;
import com.stolbunov.roman.homework_22.model.movie_categories.MovieCategoryFactory;

import java.util.List;

public class EmulatorDB {
    private static EmulatorDB db;
    private static List<MovieCategory> listMovieCategory;


    private EmulatorDB() {
    }

    public static EmulatorDB getInstance() {
        if (db == null) {
            db = new EmulatorDB();
            createDB();
        }
        return db;
    }

    private static void createDB() {
        listMovieCategory = MovieCategoryFactory.createMovieCategories();
    }

    public List<Movie> getMovies() {
        MovieCategory category = listMovieCategory.get((int) (Math.random() * listMovieCategory.size()));
        return category.getMovies();
    }

    public List<MovieCategory> getMovitCategory() {
        return listMovieCategory;
    }

}
