package com.stolbunov.roman.homework_22.ui.fragments;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

public interface OnWatchButtonClickListener {
    void onWatchClick(Movie movie);
}
