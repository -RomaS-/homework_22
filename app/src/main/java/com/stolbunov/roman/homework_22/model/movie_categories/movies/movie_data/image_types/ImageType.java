package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.image_types;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageType implements Parcelable {
    private String horizontalURI;
    private String verticalURI;

    public ImageType(String horizontalURI, String verticalURI) {
        this.horizontalURI = horizontalURI;
        this.verticalURI = verticalURI;
    }

    protected ImageType(Parcel in) {
        horizontalURI = in.readString();
        verticalURI = in.readString();
    }

    public static final Creator<ImageType> CREATOR = new Creator<ImageType>() {
        @Override
        public ImageType createFromParcel(Parcel in) {
            return new ImageType(in);
        }

        @Override
        public ImageType[] newArray(int size) {
            return new ImageType[size];
        }
    };

    public String getHorizontalURI() {
        return horizontalURI;
    }

    public String getVerticalURI() {
        return verticalURI;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(horizontalURI);
        dest.writeString(verticalURI);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
