package com.stolbunov.roman.homework_22.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

import java.util.List;

public class UserMoviesAdapter extends RecyclerView.Adapter<UserMoviesAdapter.UserMoviesViewHolder> {
    private OnMovieItemClickListener listener;
    private List<Movie> movies;

    public UserMoviesAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public UserMoviesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_user_movies, viewGroup, false);
        return new UserMoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserMoviesViewHolder userMoviesViewHolder, int position) {
        Movie movie = movies.get(position);
        userMoviesViewHolder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setOnMovieItemClickListener(OnMovieItemClickListener listener) {
        this.listener = listener;
    }

    class UserMoviesViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView image;
        AppCompatTextView name;
        AppCompatTextView data;
        AppCompatButton btnWatch;

        public UserMoviesViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.user_movies_image);
            name = itemView.findViewById(R.id.user_movies_name);
            data = itemView.findViewById(R.id.user_movies_data);
            btnWatch = itemView.findViewById(R.id.user_movies_btn_watch);
        }

        public void bind(Movie movie) {
            btnWatch.setOnClickListener((v -> watchNowClick(movie)));
            loadMovieImage(movie);
            name.setText(movie.getName());
            data.setText(movie.getData());
        }

        private void watchNowClick(Movie movie) {
            if (listener != null) {
                listener.onMovieItemClick(movie);
            }
        }

        private void loadMovieImage(Movie movie) {
            Glide.with(itemView.getContext()).load(movie.getImageType().getVerticalURI()).into(image);
        }
    }
}
