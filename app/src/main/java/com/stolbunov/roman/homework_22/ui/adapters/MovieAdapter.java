package com.stolbunov.roman.homework_22.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private List<Movie> movies;
    private OnMovieItemClickListener movieItemClickListener;

    public MovieAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_one_movie, viewGroup, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder movieViewHolder, int position) {
        Movie movie = movies.get(position);
        movieViewHolder.bind(movie);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void setMovieItemClickListener(OnMovieItemClickListener movieItemClickListener) {
        this.movieItemClickListener = movieItemClickListener;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView movieImage;
        AppCompatTextView movieName;

        public MovieViewHolder(@NonNull View itemView) {
            super(itemView);

            movieName = itemView.findViewById(R.id.movie_name);
            movieImage = itemView.findViewById(R.id.movie_image);
        }

        public void bind(Movie movie) {
            loadMovieImage(movie);
            movieName.setText(movie.getName());

            itemView.setOnClickListener((v -> onMovieItemClick(movie)));
        }

        private void loadMovieImage(Movie movie) {
            Glide.with(itemView.getContext())
                    .load(movie.getImageType().getVerticalURI())
                    .into(movieImage);
        }

        private void onMovieItemClick(Movie movie) {
            if (movieItemClickListener != null) {
                movieItemClickListener.onMovieItemClick(movie);
            }
        }
    }
}
