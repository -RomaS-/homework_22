package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_description;

public class MovieDescriptionFactory {
    private static String shortDescription = "Draenor, the homeworld of the orcs, is being torn " +
            "apart by a mysterious force known as fel magic. Gul'dan, a powerful orc warlock, unites" +
            " the orc clans and forms the Horde, and creates a portal to the world of Azeroth.";

    private static String detailedDescription = "Draenor, the homeworld of the orcs, is being torn" +
            " apart by a mysterious force known as fel magic. Gul'dan, a powerful orc warlock, " +
            "unites the orc clans and forms the Horde, and creates a portal to the world of Azeroth." +
            " The orcs begin to use fel magic to drain the life out of captive draenei in order to " +
            "sustain the portal. Once it is operational, Gul'dan leads a small warband to capture" +
            " prisoners on Azeroth and sacrifice them to bring the rest of the Horde through the portal." +
            " Durotan, the chieftain of the Frostwolf Clan, his pregnant mate Draka, and his friend" +
            " Orgrim Doomhammer join this initial warband. While crossing through the portal, Draka" +
            " goes into labor. When the orcs arrive on Azeroth, Gul'dan assists Draka with giving" +
            " birth, but while the baby is born alive, it is in neonatal distress, struggling to" +
            " breathe, and is near death. Gul'dan then drains the life out of a nearby deer to" +
            " revive and infuse fel magic into the baby, which Durotan later names Go'el.";


    public static MovieDescription createMovieDescription() {
        return new MovieDescription(shortDescription, detailedDescription);
    }
}
