package com.stolbunov.roman.homework_22.model.movie_categories.movies;

import android.os.Parcel;
import android.os.Parcelable;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.image_types.ImageType;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_description.MovieDescription;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_similar.SimilarMovies;

public class Movie implements Parcelable {
    private String name;
    private String data;
    private ImageType imageType;
    private MovieDescription movieDescription;
    private SimilarMovies similarMovies;

    public Movie(String name, String data, ImageType imageType, MovieDescription movieDescription, SimilarMovies similarMovies) {
        this.name = name;
        this.data = data;
        this.imageType = imageType;
        this.movieDescription = movieDescription;
        this.similarMovies = similarMovies;
    }

    protected Movie(Parcel in) {
        name = in.readString();
        data = in.readString();
        imageType = in.readParcelable(MovieDescription.class.getClassLoader());
        movieDescription = in.readParcelable(MovieDescription.class.getClassLoader());
        similarMovies = in.readParcelable(SimilarMovies.class.getClassLoader());
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(data);
        dest.writeParcelable(imageType, flags);
        dest.writeParcelable(movieDescription, flags);
        dest.writeParcelable(similarMovies, flags);
    }

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public MovieDescription getMovieDescription() {
        return movieDescription;
    }

    public SimilarMovies getSimilarMovies() {
        return similarMovies;
    }
}
