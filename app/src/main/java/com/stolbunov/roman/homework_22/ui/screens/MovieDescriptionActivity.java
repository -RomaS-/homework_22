package com.stolbunov.roman.homework_22.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

public class MovieDescriptionActivity extends AppCompatActivity {
    public static final String KEY_MOVIE_INTENT = "KEY_MOVIE_INTENT";

    private AppCompatImageView movieImage;
    private AppCompatTextView movieName;
    private AppCompatTextView movieData;
    private AppCompatTextView movieShortDescription;
    private AppCompatTextView movieType;
    private RoundedImageView similarMovie_1;
    private RoundedImageView similarMovie_2;
    private RoundedImageView similarMovie_3;
    private RoundedImageView similarMovie_4;


    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_description);

        initToolbar();
        initMovieFromIntent();
        initView();
        fillingView();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tb_movie_description, menu);
        return true;
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.movie_descr_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

    }

    public void initMovieFromIntent() {
        Intent intent = getIntent();
        movie = (Movie) intent.getParcelableExtra(KEY_MOVIE_INTENT);
    }

    private void initView() {
        movieImage = findViewById(R.id.md_iv_movie_image);
        movieName = findViewById(R.id.md_movie_name);
        movieData = findViewById(R.id.md_movie_data);
        movieShortDescription = findViewById(R.id.md_short_description);
        movieType = findViewById(R.id.md_movie_type);
        similarMovie_1 = findViewById(R.id.md_similar_movie_1);
        similarMovie_2 = findViewById(R.id.md_similar_movie_2);
        similarMovie_3 = findViewById(R.id.md_similar_movie_3);
        similarMovie_4 = findViewById(R.id.md_similar_movie_4);
        AppCompatButton btnReedMore = findViewById(R.id.md_btn_read_more);
        btnReedMore.setOnClickListener(this::readMoreClick);
    }

    private void fillingView() {
        loadImage(movie.getImageType().getHorizontalURI(), movieImage);
        movieName.setText(movie.getName());
        movieData.setText(movie.getData());
        movieShortDescription.setText(movie.getMovieDescription().getShortDescription());
        loadImage(movie.getSimilarMovies().getMovie1(), similarMovie_1);
        loadImage(movie.getSimilarMovies().getMovie2(), similarMovie_2);
        loadImage(movie.getSimilarMovies().getMovie3(), similarMovie_3);
        loadImage(movie.getSimilarMovies().getMovie4(), similarMovie_4);
    }

    private void loadImage(String imageURI, ImageView imageView) {
        Glide.with(this).load(imageURI).into(imageView);
    }

    private void readMoreClick(View view) {

    }
}
