package com.stolbunov.roman.homework_22.ui.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.SparseArrayCompat;

import com.stolbunov.roman.homework_22.ui.fragments.AbstractTabFragment;
import com.stolbunov.roman.homework_22.ui.fragments.MoviesFragment;
import com.stolbunov.roman.homework_22.ui.fragments.TVShowsFragment;

import java.util.Objects;

public class TabUserLibraryFragmentAdapter extends FragmentPagerAdapter {
    private SparseArrayCompat<AbstractTabFragment> tabFragments;

    public TabUserLibraryFragmentAdapter(Context context, FragmentManager fm) {
        super(fm);
        createTabFragments(context);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return Objects.requireNonNull(tabFragments.get(position)).getTitle();
    }

    @Override
    public Fragment getItem(int i) {
        return tabFragments.get(i);
    }

    @Override
    public int getCount() {
        return tabFragments.size();
    }

    private void createTabFragments(Context context) {
        tabFragments = new SparseArrayCompat<>();
        tabFragments.put(0, MoviesFragment.newInstance(context));
        tabFragments.put(1, TVShowsFragment.newInstance(context));
    }
}
