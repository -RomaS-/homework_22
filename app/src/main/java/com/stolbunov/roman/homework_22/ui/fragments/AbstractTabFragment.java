package com.stolbunov.roman.homework_22.ui.fragments;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.ui.adapters.UserMoviesAdapter;

import java.util.List;

public abstract class AbstractTabFragment extends Fragment {
    protected String title;
    protected OnWatchButtonClickListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (!(context instanceof OnWatchButtonClickListener)) {
            throw new IllegalStateException("Implement OnWatchButtonClickListener");
        }
        listener = (OnWatchButtonClickListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @NonNull
    protected LinearLayoutManager getManager() {
        return new LinearLayoutManager(
                getContext(), LinearLayoutManager.VERTICAL, false);
    }

    @NonNull
    protected UserMoviesAdapter getAdapter(List<Movie> movies) {
        UserMoviesAdapter adapter = new UserMoviesAdapter(movies);
        adapter.setOnMovieItemClickListener(this::onWatch);
        return adapter;
    }

    protected void onWatch(Movie movie) {
        if (listener != null) {
            listener.onWatchClick(movie);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
