package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_description;

import android.os.Parcel;
import android.os.Parcelable;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

public class MovieDescription implements Parcelable {
    private String shortDescription;
    private String detailedDescription;

    public MovieDescription(String shortDescription, String detailedDescription) {
        this.shortDescription = shortDescription;
        this.detailedDescription = detailedDescription;
    }

    protected MovieDescription(Parcel in) {
        shortDescription = in.readString();
        detailedDescription = in.readString();
    }

    public static final Creator<MovieDescription> CREATOR = new Creator<MovieDescription>() {
        @Override
        public MovieDescription createFromParcel(Parcel in) {
            return new MovieDescription(in);
        }

        @Override
        public MovieDescription[] newArray(int size) {
            return new MovieDescription[size];
        }
    };

    public String getShortDescription() {
        return shortDescription;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shortDescription);
        dest.writeString(detailedDescription);
    }
}
