package com.stolbunov.roman.homework_22.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.model.movie_categories.MovieCategory;

import java.util.List;

public class MovieCategoriesAdapter extends RecyclerView.Adapter<MovieCategoriesAdapter.MovieCategoryViewHolder> {
    private RecyclerView.LayoutManager layoutManager;

    private OnCategoryItemClickListener categoryItemClickListener;
    private List<MovieCategory> movieCategories;

    public MovieCategoriesAdapter(List<MovieCategory> movieCategories) {
        this.movieCategories = movieCategories;
    }

    @NonNull
    @Override
    public MovieCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_movie_category, viewGroup, false);
        createLayoutManagerForItemCategory(viewGroup);
        return new MovieCategoryViewHolder(view);
    }

    private void createLayoutManagerForItemCategory(@NonNull ViewGroup viewGroup) {
        layoutManager = new LinearLayoutManager(
                viewGroup.getContext(), LinearLayoutManager.HORIZONTAL, false);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCategoryViewHolder movieCategoryViewHolder, int position) {
        MovieCategory category = movieCategories.get(position);
        movieCategoryViewHolder.bind(category);
    }

    @Override
    public int getItemCount() {
        return movieCategories.size();
    }

    public void setCategoryItemClickListener(OnCategoryItemClickListener categoryItemClickListener) {
        this.categoryItemClickListener = categoryItemClickListener;
    }

    class MovieCategoryViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView titleMovieCategory;
        AppCompatTextView titleViewAll;
        RecyclerView movieContainer;

        MovieAdapter adapter;

        public MovieCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            titleMovieCategory = itemView.findViewById(R.id.title_movie_category);
            titleViewAll = itemView.findViewById(R.id.title_view_all);
            movieContainer = itemView.findViewById(R.id.rv_movie_container);
        }

        public void bind(MovieCategory category) {
            adapter = new MovieAdapter(category.getMovies());
            adapter.setMovieItemClickListener(this::onChildItemClick);

            titleViewAll.setOnClickListener((v -> viewAllClick(category)));

            titleMovieCategory.setText(category.getCategoryName());

            createMovieContainer();
        }

        private void createMovieContainer() {
            movieContainer.setLayoutManager(layoutManager);
            movieContainer.setAdapter(adapter);
        }

        private void onChildItemClick(Movie movie) {
            if (categoryItemClickListener != null) {
                categoryItemClickListener.categoryChildItemClick(movie);
            }
        }

        private void viewAllClick(MovieCategory category) {
            if (categoryItemClickListener != null) {
                categoryItemClickListener.categoryItemClick(category);
            }

        }
    }
}
