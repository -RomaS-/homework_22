package com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_similar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SimilarMoviesFactory {

    public static SimilarMovies createSimilarMovies(int indexTargetMovie, List<String> listURI) {
        List<String> list = new ArrayList<>(listURI);
        list.remove(indexTargetMovie);
        Set<String> dataForSimilarMovies = new HashSet<>();

        while (dataForSimilarMovies.size() < 4) {
            int index = (int) (Math.random() * list.size());
            dataForSimilarMovies.add(list.get(index));
        }

        return new SimilarMovies(dataForSimilarMovies);
    }
}
