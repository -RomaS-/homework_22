package com.stolbunov.roman.homework_22.ui.adapters;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

public interface OnMovieItemClickListener {
    void onMovieItemClick(Movie movie);
}
