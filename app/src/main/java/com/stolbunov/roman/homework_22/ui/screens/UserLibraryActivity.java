package com.stolbunov.roman.homework_22.ui.screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.stolbunov.roman.homework_22.R;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.ui.adapters.TabUserLibraryFragmentAdapter;
import com.stolbunov.roman.homework_22.ui.fragments.OnWatchButtonClickListener;

public class UserLibraryActivity extends AppCompatActivity implements OnWatchButtonClickListener {
    public static final String KEY_WATCH_CLICK = "KEY_WATCH_CLICK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_library);

        initToolbar();
        initTabLayout(getViewPager());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tb_user_library, menu);
        return true;
    }

    @Override
    public void onWatchClick(Movie movie) {
        Intent intent = new Intent(this, MovieDescriptionActivity.class);
        intent.putExtra(MovieDescriptionActivity.KEY_MOVIE_INTENT, movie);
        startActivity(intent);
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.user_library_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_down);
    }

    @NonNull
    private ViewPager getViewPager() {
        ViewPager pager = findViewById(R.id.user_library_view_pager);
        pager.setAdapter(new TabUserLibraryFragmentAdapter(this, getSupportFragmentManager()));
        return pager;
    }

    private void initTabLayout(ViewPager pager) {
        TabLayout tabLayout = findViewById(R.id.user_library_tab_layout);
        tabLayout.setupWithViewPager(pager);
    }
}
