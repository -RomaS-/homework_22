package com.stolbunov.roman.homework_22.model.movie_categories.movies;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.image_types.ImageTypeFactory;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_description.MovieDescriptionFactory;
import com.stolbunov.roman.homework_22.model.movie_categories.movies.movie_data.movie_similar.SimilarMoviesFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MoviesFactory {
    private static List<String> names;
    private static List<String> data;

    static {
        creatingMovieName();
        creatingMovieData();
    }



    public static List<Movie> createMovies() {
        List<Movie> movies = new LinkedList<>();
        for (int i = 0; i < names.size(); i++) {
            movies.add(new Movie(
                    names.get(i),
                    data.get(i),
                    ImageTypeFactory.createImageType(i),
                    MovieDescriptionFactory.createMovieDescription(),
                    SimilarMoviesFactory.createSimilarMovies(i, ImageTypeFactory.getImagesUriVertical())));
        }
        Collections.shuffle(movies);
        return movies;
    }

    private static void creatingMovieName() {
        names = new LinkedList<>();
        names.add("Rampage");
        names.add("Black Panther");
        names.add("Rise of the Guardians");
        names.add("Solo: A Star Wars Story");
        names.add("Deadpool 2");
        names.add("House");
        names.add("The Big Bang Theory");
        names.add("Fantastic Beasts: The Crimes of Grindelwald");
        names.add("Warcraft");
        names.add("Bumblebee");
        names.add("Doctor Strange");
        names.add("Transformers: Dark of the Moon");
        names.add("The Croods");
        names.add("Just Go with It");
        names.add("Venom");
        names.add("Blended");
        names.add("Avatar");
    }

    private static void creatingMovieData() {
        data = new LinkedList<>();
        data.add("2018, 149 mins");
        data.add("2018, 134 mins");
        data.add("2012, 97 mins");
        data.add("2018, 135 mins");
        data.add("2018, 119 mins");
        data.add("2004, 23 mins");
        data.add("2007, 22 mins");
        data.add("2018, 134 mins");
        data.add("2016, 123 mins");
        data.add("2018, 138 mins");
        data.add("2016, 115 mins");
        data.add("2011, 154 mins");
        data.add("2013, 98 mins");
        data.add("2011, 117 mins");
        data.add("2018, 112 mins");
        data.add("2014, 117 mins");
        data.add("2009, 161 mins");
    }
}
