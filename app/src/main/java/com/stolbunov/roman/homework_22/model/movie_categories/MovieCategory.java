package com.stolbunov.roman.homework_22.model.movie_categories;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;

import java.util.List;

public class MovieCategory {
    private String categoryName;
    private List<Movie> movies;

    public MovieCategory(String categoryName, List<Movie> movies) {
        this.categoryName = categoryName;
        this.movies = movies;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addNewMovie(Movie movie) {
        movies.add(movie);
    }

    public void removeMoviewByID(int movieID) {
        movies.remove(movieID);
    }
}
