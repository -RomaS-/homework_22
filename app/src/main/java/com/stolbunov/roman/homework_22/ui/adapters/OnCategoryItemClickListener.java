package com.stolbunov.roman.homework_22.ui.adapters;

import com.stolbunov.roman.homework_22.model.movie_categories.movies.Movie;
import com.stolbunov.roman.homework_22.model.movie_categories.MovieCategory;

public interface OnCategoryItemClickListener {
    void categoryChildItemClick(Movie movie);

    void categoryItemClick(MovieCategory category);
}
